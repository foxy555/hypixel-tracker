import requests
import logging
import os
import sys
import time
import threading
from hypixel import Hypixel
from sessionWatcher import SessionWatcher
from playerWatcher import PlayerWatcher
from gameWatcher import GameWatcher


def main():
    if not os.path.exists("apikey"):
        print("apikey not found")
        sys.exit(1)

    f = open("apikey", "r")
    apikey = f.read().strip()
    name = ""
    if os.path.exists("name"):
        f = open("name", "r")
        name = f.read().strip()
        f.close()
    else:
        name = input("Enter Name: ")
        f = open("name", "w")
        f.write(name)
        f.close()

    sessionWatcher = SessionWatcher(name, apikey)
    playerWatcher = PlayerWatcher(name, apikey)
    gameWatcher = GameWatcher(name, apikey)

    tS = threading.Thread(target=sessionW, args=(sessionWatcher,))
    tP = threading.Thread(target=playerW, args=(playerWatcher,))
    tG = threading.Thread(target=gameW, args=(gameWatcher,))

    tS.start()
    tP.start()
    tG.start()
    print("Threads started")
    tS.join()
    tP.join()
    tG.join()


def sessionW(sessionWatcher):
    while True:
        sessionWatcher.pulse()
        time.sleep(30)


def playerW(playerWatcher):
    while True:
        playerWatcher.pulse()
        time.sleep(86400)


def gameW(gameWatcher):
    while True:
        gameWatcher.pulse()
        time.sleep(600)


if __name__ == "__main__":
    main()
