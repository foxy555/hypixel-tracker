import logging
import requests


class Hypixel:
    def __init__(self, api):
        self.api = api

    def setPlayerName(self, name):
        response = requests.get(
            "https://api.mojang.com/users/profiles/minecraft/" + name)
        response.raise_for_status()
        self.uuid = response.json()["id"]
        pass

    def setPlayerUUID(self, uuid):
        self.uuid = uuid

    def getAPI(self, method, args):
        try:
            response = requests.get(
                "https://api.hypixel.net/" + method + "?" + "&".join(args))
        except Exception as e:
            print(e)
        if(response.status_code != 200):
            logging.error("Request failed with code " +
                          str(response.status_code))
            return None
        return response

    def getStatus(self):
        response = self.getAPI(
            "status", ["key=" + self.api, "uuid=" + self.uuid])
        if response == None:
            return None
        response = response.json()
        if(response["success"]):
            return response["session"]
        return None

    def getPlayer(self):
        response = self.getAPI(
            "player", ["key=" + self.api, "uuid=" + self.uuid])
        if response == None:
            return None
        response = response.json()
        if(response["success"]):
            return response["player"]
        return None

    def getRecentGames(self):
        response = self.getAPI(
            "recentGames", ["key=" + self.api, "uuid=" + self.uuid])
        if response == None:
            return None
        response = response.json()
        if (response["success"]):
            return response["games"]
        return None
